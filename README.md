# 🗞️ ScopeAI

We recommand the usage of [Yarn](https://yarnpkg.com) instead of [NPM](https://www.npmjs.com).

### Installation

```pwsh
$ yarn install
yarn install v1.22.19
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Saved lockfile.
Done in 11.46s.
```

### Usage

> By default, the API will listen the 3000 port.

#### With CLI

```pwsh
# Start the server
$ yarn serve
{"level":30,"time":1684222189067,"pid":6808,"hostname":"xxx","msg":"Server listening at http://127.0.0.1:3000"}
{"level":30,"time":1684222189071,"pid":6808,"hostname":"xxx","msg":"Server listening at http://[::1]:3000"}

# Start the server in dev mode (it'll refresh each time the code files is modified)
$ yarn dev
[nodemon] 2.0.22
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node index.js`
{"level":30,"time":1684222189067,"pid":6808,"hostname":"xxx","msg":"Server listening at http://127.0.0.1:3000"}
{"level":30,"time":1684222189071,"pid":6808,"hostname":"xxx","msg":"Server listening at http://[::1]:3000"}
```

#### With Docker

You can pull a specific version of the API by replacing *latest* with the [repository tags](https://gitlab.com/stanislasbdx/scopeai/-/tags).

```pwsh
# Running with a specified and fullfilled .env file
$ docker run -d -p 3000:3000 --env-file .env registry.gitlab.com/stanislasbdx/scopeai/main:latest
latest: Pulling from stanislasbdx/scopeai/main
...
56618218a026ac388bcda3aeb30977ef671d593ddfc7dacc8a828131f14f953b

# Running with specific environnement variables
$ docker run -d -p 3000:3000 --e OPENAI_API_KEY=<oai_key> --e OPENAI_ORGANIZATION=<oai_org> --e NEWSAPI_KEY=<newsapi_key> registry.gitlab.com/stanislasbdx/scopeai/main:latest
latest: Pulling from stanislasbdx/scopeai/main
...
56618218a026ac388bcda3aeb30977ef671d593ddfc7dacc8a828131f14f953b
```
