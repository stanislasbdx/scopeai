ARG NODE_VERSION=18

# Stage 1: Build the application
FROM node:${NODE_VERSION}-alpine as builder

RUN apk update && apk upgrade && \
    apk add --no-cache curl bash npm && \
    npm install --global --force npm@latest yarn@latest

RUN yarn global add npm@latest node-prune

## Install node-prune (https://github.com/tj/node-prune)
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

WORKDIR /app

COPY . .
RUN yarn install --production --no-lockfile --ignore-optional

RUN node-prune

# Stage 2: Create the final image
FROM node:${NODE_VERSION}-alpine

WORKDIR /app

COPY --from=builder /app .

EXPOSE 3000
CMD ["yarn", "serve"]