const Configuration = require("openai").Configuration;
const OpenAIApi = require("openai").OpenAIApi;

const configuration = new Configuration({
	organization: "org-kiIV68tUCxfNDvhCoRaiQ7Ki",
	apiKey: process.env.OPENAI_API_KEY,
});

async function getEngines() {
	const openai = new OpenAIApi(configuration);
	return await openai.listModels();
}

async function isArticlesRelevant(prompt, articles) {
	if (!process.env.OPENAI_ORGANIZATION || !process.env.OPENAI_API_KEY)
		throw new Error(
			"No OPENAI_ORGANIZATION or OPENAI_API_KEY provided in .env"
		);

	const configuration = new Configuration({
		organization: process.env.OPENAI_ORGANIZATION,
		apiKey: process.env.OPENAI_API_KEY,
	});

	const openai = new OpenAIApi(configuration);

	const response = await openai.createChatCompletion({
		model: process.env.OPENAI_MODEL,
		messages: [
			{
				role: "system",
				content: "Your answer must be in this JSON form : { percentage: Number, comment: String}",
			},
			{
				role: "user",
				content: `Gives an index (percentage) on 100% and a comment (comment) on the relevance of the list of articles. : ${articles} in relation to the prompt : ${prompt}`,
			},
		],
	});

	return JSON.parse(response.data.choices[0].message.content);
}

async function compactArticles(prompt, articles) {
	if (!process.env.OPENAI_ORGANIZATION || !process.env.OPENAI_API_KEY)
		throw new Error(
			"No OPENAI_ORGANIZATION or OPENAI_API_KEY provided in .env"
		);

	const configuration = new Configuration({
		organization: process.env.OPENAI_ORGANIZATION,
		apiKey: process.env.OPENAI_API_KEY,
	});

	const openai = new OpenAIApi(configuration);

	const response = await openai.createChatCompletion({
		model: process.env.OPENAI_MODEL,
		messages: [{
			role: "user",
			content: `Thoses are articles : ${prompt}, summarize them : ${articles}`,
		}, ],
	});

	const relevancy = await isArticlesRelevant(prompt, articles);

	return {
		summary: response.data.choices[0].message.content,
		relevance: relevancy
	};
}

async function getNews(topic) {
	const news = await fetch(`https://newsapi.org/v2/everything?q=${topic}&apiKey=${process.env.NEWSAPI_KEY}&pageSize=10&sortBy=relevancy`)
		.then((response) => response.json())
		.catch((error) => {
			throw Error(`Error fetching articles : ${error}`);
		});

	var articles = [];
	if (news.totalResults > 0) news.articles.forEach((article) => {
		articles.push({
			...article,
			source: article.source.name,
			publishedAt: new Date(article.publishedAt)
		});
	});

	return articles;
}

module.exports = {
	getEngines,
	getNews,
	compactArticles,
};