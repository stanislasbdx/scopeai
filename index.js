const dotenv = require("dotenv");
dotenv.config();

const api = require("fastify")({ logger: true });

api.register(require("./routes/articles"), { prefix: "/articles" });

api.get("/ping", async (request, reply) => {
	return "Pong!";
});

const start = async () => {
	try {
		await api.listen({ port: 3000, host: "0.0.0.0" });
	} catch (err) {
		api.log.error(err);
		process.exit(1);
	}
};

start();
