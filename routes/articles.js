const { getEngines, getNews, compactArticles } = require("../functions");

module.exports = function (api, opts, done) {
	api.get("/query/:query", async (request, reply) => {
		return await getNews(request.params.query);
	});

	api.get("/compile/:query", async (request, reply) => {
		const articles = await getNews(request.params.query);

		return {
			...(await compactArticles(
				request.params.query,
				JSON.stringify(articles)
			)),
			articles
		};
	});

	done();
};